// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

//import { Router, Route } from '@redwoodjs/router'
import NewPersonPage from 'src/pages/NewPersonPage'
import PeoplePage from 'src/pages/PeoplePage'
import EditPersonPage from 'src/pages/EditPersonPage'
import PersonPage from 'src/pages/PersonPage'
import HomePage from 'src/pages/HomePage'
// import SearchPeoplePage from 'src/pages/SearchPeoplePage'
import {
  BrowserRouter,
  Switch,
  Route as ReactRoute,
} from "react-router-dom";

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <ReactRoute path="/people/new">
          <NewPersonPage />
        </ReactRoute>
        {/* <ReactRoute path="/people/search">
          <SearchPeoplePage />
        </ReactRoute> */}
        <ReactRoute path="/people/:id/edit">
          <EditPersonPage />
        </ReactRoute>
        <ReactRoute path="/people/:id">
          <PersonPage />
        </ReactRoute>
        <ReactRoute path="/people">
          <PeoplePage />
        </ReactRoute>
        <ReactRoute path="/">
          <HomePage />
        </ReactRoute>
      </Switch>
    </BrowserRouter>
    // <Router>
    //   <Route path="/people/new" page={NewPersonPage} name="newPerson" />
    //   <Route path="/people/{id}/edit" page={EditPersonPage} name="editPerson" />
    //   <Route path="/people/{id}" page={PersonPage} name="person" />
    //   <Route path="/people" page={PeoplePage} name="people" />
    //   <Route path="/people/anything" page={NewPersonPage} name="anything" />
    //   <Route path="/" page={HomePage} name="home" />
    //   <Route notfound page={NotFoundPage} />
    // </Router>
  )
}

export default Routes
