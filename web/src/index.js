import ReactDOM from 'react-dom'
import { RedwoodProvider, FatalErrorBoundary } from '@redwoodjs/web'
import { initializeIcons } from '@uifabric/icons';
import { loadTheme } from '@uifabric/styling';
import FatalErrorPage from 'src/pages/FatalErrorPage'

import ReactRoutes from 'src/ReactRoutes'

import '@fortawesome/fontawesome-free/js/all'
import './scaffold.css'
import './index.scss'

loadTheme({
  defaultFontStyle: { fontFamily: 'Muli, \'Noto Sans HK\', sans-serif', fontWeight: 'regular' },
  // fonts: {
  // },
  palette: {
    themePrimary: '#557d28',
    themeLighterAlt: '#030502',
    themeLighter: '#0e1406',
    themeLight: '#1a250c',
    themeTertiary: '#334b18',
    themeSecondary: '#4b6e23',
    themeDarkAlt: '#638a36',
    themeDark: '#769c4b',
    themeDarker: '#96b671',
    neutralLighterAlt: '#f8f8f8',
    neutralLighter: '#f4f4f4',
    neutralLight: '#eaeaea',
    neutralQuaternaryAlt: '#dadada',
    neutralQuaternary: '#d0d0d0',
    neutralTertiaryAlt: '#c8c8c8',
    neutralTertiary: '#a19f9d',
    neutralSecondary: '#605e5c',
    neutralPrimaryAlt: '#3b3a39',
    neutralPrimary: '#323130',
    neutralDark: '#201f1e',
    black: '#000000',
    white: '#ffffff',
  }
});

initializeIcons();

// const client = new ApolloClient({
//   uri: 'http://localhost:8911/graphql/',
// });

ReactDOM.render(
  <FatalErrorBoundary page={FatalErrorPage}>
    <RedwoodProvider>
      {/* <ApolloProvider client={client}> */}
        <ReactRoutes />
      {/* </ApolloProvider> */}
    </RedwoodProvider>
  </FatalErrorBoundary>,
  document.getElementById('redwood-app')
)
