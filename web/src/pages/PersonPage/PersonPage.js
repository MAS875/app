import {
  useParams
} from "react-router-dom";
import AdminLayout from 'src/layouts/AdminLayout'
import Person from 'src/components/Person'

const PersonPage = () => {
  let { id } = useParams();

  return (
    <AdminLayout title="會友資料">
      <Person id={id} />
    </AdminLayout>
  )
}

export default PersonPage
