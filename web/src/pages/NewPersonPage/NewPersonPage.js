import AdminLayout from 'src/layouts/AdminLayout'
import NewPerson from 'src/components/NewPerson'

const NewPersonPage = (props) => {
  return (
    <AdminLayout name="newPerson" title="新增會友">
      <NewPerson />
    </AdminLayout>
  )
}

export default NewPersonPage
