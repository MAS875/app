// import { navigate, routes } from '@redwoodjs/router'
import { useHistory } from "react-router-dom";
import AdminLayout from 'src/layouts/AdminLayout'
import PeopleCell from 'src/components/PeopleCell'
import { InfiniteGrid } from 'src/components/InfiniteGrid/InfiniteGrid'
import { displayData } from 'src/components/DisplayData'
import {
  Stack,
  DefaultButton,
} from '@fluentui/react';

export const QUERY = gql`
  query PEOPLE($orderBy: PersonOrderByInput, $offset: Int!, $limit: Int!) {
    people(orderBy: $orderBy, offset: $offset, limit: $limit) {
      pageInfo {
        totalCount
      }
      nodes {
        id
        surname
        givenName
        surnameEn
        givenNameEn
        gender
        joinChurchOn
        baptismOn
      }
    }
  }
`

const DELETE_PERSON_MUTATION = gql`
  mutation DeletePersonMutation($id: String!) {
    deletePerson(id: $id) {
      id
    }
  }
`

// const [deletePerson] = useMutation(DELETE_PERSON_MUTATION)

// const onDeleteClick = (id) => {
//   if (confirm('Are you sure you want to delete person ' + id + '?')) {
//     deletePerson({ variables: { id }, refetchQueries: ['POSTS'] })
//   }
// }

const columns = history => ([
  {
    field: '姓名',
    value: data => displayData(data.surname) + displayData(data.givenName),
    width: 150,
    orderBy: 'surname',
  },
  {
    field: '英文姓名',
    value: data => displayData(data.surnameEn) + ' ' + displayData(data.givenNameEn),
    width: 220,
    orderBy: 'surnameEn',
  },
  {
    field: '性別',
    value: data => data.gender,
    type: 'gender',
    width: 100,
    orderBy: 'gender',
  },
  {
    field: '信仰狀況',
    value: data => !!data.baptismOn,
    type: 'christianStatus',
    width: 120,
    orderBy: 'baptismOn',
  },
  {
    field: '加入教會日期',
    value: data => data.joinChurchOn,
    type: 'date',
    width: 220,
    orderBy: 'joinChurchOn',
  },
  // {
  //   field: '',
  //   view: id => (
  //     <Stack horizontal>
  //       <DefaultButton text="詳情" onClick={() => history.push(`/people/${id}`)} />
  //       <DefaultButton text="更改" onClick={() => history.push(`/people/${id}/edit`)} />
  //       {/* <DefaultButton text="刪除" onClick={() => onDeleteClick(id)} /> */}
  //     </Stack>
  //   ),
  //   width: 200,

  // },
])

const PeoplePage = () => {
  const headerRef = React.createRef()
  const history = useHistory();

  const handleScroll = e => {
    let element = e.target

    headerRef.current.scrollLeft = element.scrollLeft
  }

  const onItemClicked = (row) => { history.push(`/people/${row.id}`) }

  return (
    <AdminLayout name="people" title="會友名冊">
      {/* <PeopleCell orderBy={{ name: 'asc' }} offset={1} limit={2} /> */}
      <InfiniteGrid query={QUERY} columns={columns(history)} onRowClicked={onItemClicked} />
      {/* <div style={{ display: 'flex', width: '100%', background: 'red', overflowX: 'hidden' }} ref={headerRef}>
      <div style={{ width: 1000 }}> content</div>
        <div style={{ flex: '0 0 100px', width: '100px', background: 'blue' }}>1</div>
        <div style={{ flex: '1 0 300px', background: 'green' }}>2</div>
        <div style={{ flex: '0 0 200px', width: '300px', background: 'brown' }}>3</div>
      </div>
      <div style={{ display: 'flex', width: '100%', background: 'red', overflowX: 'auto' }} onScroll={handleScroll}>
        <div style={{ flex: '0 0 100px', width: '100px', background: 'blue' }}>1sdfsdfsdfsdfsfdsf</div>
        <div style={{ flex: '1 0 300px', background: 'green' }}>2</div>
        <div style={{ flex: '0 0 200px', width: '300px', background: 'brown' }}>3</div>
      </div> */}
    </AdminLayout>
  )
}

export default PeoplePage
