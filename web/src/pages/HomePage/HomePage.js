import AdminLayout from "src/layouts/AdminLayout/AdminLayout"

const HomePage = () => {
  return (
    <AdminLayout name="home" title="首頁">
      <div>
        <h1>HomePage</h1>
      </div>
    </AdminLayout>
  )
}

export default HomePage
