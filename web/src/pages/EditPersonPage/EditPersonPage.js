import {
  useParams
} from "react-router-dom";
import AdminLayout from 'src/layouts/AdminLayout'
import EditPersonCell from 'src/components/EditPersonCell'

const EditPersonPage = () => {
  let { id } = useParams();

  return (
    <AdminLayout title="更新會友資料">
      <EditPersonCell id={id} />
    </AdminLayout>
  )
}

export default EditPersonPage
