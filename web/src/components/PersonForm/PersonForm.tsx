import * as React from 'react'
import {
  ITextFieldStyles,
  Stack,
  DefaultButton,
  PrimaryButton,
} from '@fluentui/react';
import { SectionCardContainer, SectionCard } from 'src/components/SectionCard';
import {
  Form,
  InlineFields,
  TextField,
  RadioField,
  DateField,
  ButtonGroupField,
  FormSubmitButton,
  ActionContainer,
  Watch,
} from 'src/components/form'

const narrowTextFieldStyles: Partial<ITextFieldStyles> = { fieldGroup: { width: 100 } };

const stackTokens = { childrenGap: 15 };

const PersonForm = ({ person, readonly, loading, onSave, onChangeClick, onDeleteClick }) => {
  const onSubmit = (data) => {
    onSave(data, person?.id)
  }

  const onChange = () => { onChangeClick(person?.id) }
  const onDelete = () => { onDeleteClick(person?.id) }

  return (
    <div className="box-border text-sm -mt-4">
      <Form readonly={readonly} loading={loading} defaultValues={person}>
        <SectionCardContainer>
          <SectionCard title="個人資料">
            <InlineFields required label="中文姓名">
              <TextField placeholder="姓氏" name="surname" required validate={{ maxLength: 4 }} />
              <TextField placeholder="名字" name="givenName" required validate={{ maxLength: 2 }} />
            </InlineFields>
            <InlineFields label="英文姓名">
              <TextField placeholder="Surname" name="surnameEn" validate={{ maxLength: 256 }} />
              <TextField placeholder="Given Name" name="givenNameEn" validate={{ maxLength: 256 }} />
            </InlineFields>
            <TextField label="身份證明編號" name="idCardNo" />
            <RadioField label="姓別" horizontal name="gender" options={[{ key: 'Male', text: '男' }, { key: 'Female', text: '女' }]} required />
            <DateField label="出生日期" name="birthday" />
          </SectionCard>
          <SectionCard title="通訊聯絡">
            <TextField label="電話號碼" name="phone" />
            <TextField label="備用電話號碼" name="phone2" />
            <TextField label="電郵地址" name="email" />
          </SectionCard>
          <SectionCard title="信仰狀況">
            <ButtonGroupField
              name="christianStatus"
              options={[
                { key: 'catechumen', text: '慕道者' },
                { key: 'converted', text: '已決志' },
                { key: 'baptized', text: '已受洗/受浸' }
              ]}
              defaultValue="catechumen"
              required
            />
            <DateField label="首次訪問教會" name="firstVisitOn" />
            <Watch name="christianStatus" defaultValue="catechumen">
              {(christianStatus: string) =>
                <>
                  {(christianStatus === 'converted' || christianStatus === 'baptized') && (
                    <>
                      <DateField label="決志日期" name="convertOn" required />
                    </>
                  )}
                  {(christianStatus === 'baptized') && (
                    <>
                      <DateField label="洗禮/受浸日期" name="baptismOn" required />
                      <DateField label="加入教會日期" name="joinChurchOn" required />
                      <TextField label="洗禮/受浸教會" name="baptismChurch" required />
                    </>
                  )}
                </>
              }
            </Watch>
          </SectionCard>
          <ActionContainer>
            {readonly ?
              <>
                <DefaultButton text="刪除" onClick={onDelete} />
                <PrimaryButton text="更新資料" onClick={onChange} />
              </> :
              <FormSubmitButton text={person ? '確定更新' : '確定新增'} onSubmit={onSubmit} />
            }
          </ActionContainer>
        </SectionCardContainer>
      </Form>
    </div>
  )
}

export default PersonForm
