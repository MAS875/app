import React, { useState, useRef, useEffect } from 'react'
import { useQuery } from '@redwoodjs/web'
import { FixedSizeList as List } from "react-window";
import InfiniteLoader from "react-window-infinite-loader"
import AutoSizer from "react-virtualized-auto-sizer"
import Skeleton from 'react-loading-skeleton'
import {
  mergeStyleSets,
} from '@fluentui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons'
import { SectionCardContainer, SectionCard } from '../SectionCard'
import { DisplayData, displayData } from '../DisplayData'

const controlClass = mergeStyleSets({
  headerRow: {
    display: 'flex',
    width: '100%',
    maxWidth: '100%',
    lineHeight: 42,
    overflowX: 'hidden',
    borderBottom: '1px solid rgb(237, 235, 233)',
  },
  headerColumn: {
    position: 'relative',
    color: 'rgb(50, 49, 48)',
    boxSizing: 'border-box',
    paddingTop: 0,
    paddingRight: 32,
    paddingBottom: 0,
    paddingLeft: 12,
    lineHeight: 'inherit',
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0,
    height: 42,
    verticalAlign: 'top',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    textAlign: 'left',
    fontSize: 12,
    fontWeight: '400',
    outline: 'transparent',
    cursor: 'pointer',
    selectors: {
      ':hover': {
        color: 'rgb(50, 49, 48)',
        background: 'rgb(243, 242, 241)',
      }
    }
  },
  headerColumnTooltip: {
    display: 'block',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  headerColumnTitle: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    boxSizing: 'border-box',
    paddingTop: 0,
    paddingRight: 8,
    paddingBottom: 0,
    paddingLeft: 12,
    outline: 'transparent',
    overflow: 'hidden',
  },
  headerColumnName: {
    textOverflow: 'ellipsis',
    fontWeight: 600,
    fontSize: 16,
    flex: '0 1 auto',
    overflow: 'hidden',
  },
  headerColumnOrderBy: {
    paddingLeft: 5,
  },
  row: {
    display: 'flex',
    width: '100%',
    maxWidth: '100%',
    alignItems: 'stretch',
    borderBottom: '1px solid rgb(243, 242, 241)',
    selectors: {
      ':hover': {
        color: 'rgb(50, 49, 48)',
        background: 'rgb(243, 242, 241)',
      }
    },
    cursor: 'pointer'
  },
  column: {
    position: 'relative',
    display: 'inline-block',
    boxSizing: 'border-box',
    minHeight: 42,
    verticalAlign: 'top',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    paddingTop: 11,
    paddingBottom: 11,
    paddingLeft: 12,
    outline: 'transparent',
    overflow: 'hidden',
  },
  rowCount: {
    textAlign: 'left',
    marginTop: 10,
    fontSize: '0.9em',
  }
})



// const loadMoreItems = (startIndex, stopIndex) => {
//   for (let index = startIndex; index <= stopIndex; index++) {
//     itemStatusMap[index] = LOADING;
//   }
//   return new Promise(resolve =>
//     setTimeout(() => {
//       for (let index = startIndex; index <= stopIndex; index++) {
//         itemStatusMap[index] = LOADED;
//       }
//       resolve();
//     }, 2500)
//   );
// };

const HeaderRow = ({ columns, getColStyle, orderBy, onOrderBy, headerRowContainerRef, headerRowRef }) => {
  return (
    <div ref={headerRowContainerRef}>
      <div className={controlClass.headerRow} ref={headerRowRef}>
        {columns.map((column, colIndex) => {
          const colStyle = getColStyle(column, colIndex)
          return (
            <div key={colIndex} className={controlClass.headerColumn} style={colStyle}>
              <span className={controlClass.headerColumnTooltip} onClick={() => onOrderBy(column.orderBy)}>
                <span className={controlClass.headerColumnTitle}>
                  <span className={controlClass.headerColumnName}>{ column.field }</span>
                  {orderBy[column.orderBy] &&
                    <span className={controlClass.headerColumnOrderBy}>
                      {(orderBy[column.orderBy] === 'asc') ?
                        <FontAwesomeIcon icon={faSortUp} /> :
                        <FontAwesomeIcon icon={faSortDown} />
                      }
                    </span>
                  }
                </span>
              </span>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export function InfiniteGrid({ query, columns, onRowClicked }) {
  const totalColWidth = columns.reduce((total, col) => total + col.width, 0)
  const colWidthPct = columns.map(col => col.width / totalColWidth * 100)

  const [ pageInfo, setPageInfo ] = useState({
    // colWidthPct: columns.map(col => col.width)
    // growColNo: columns.reduce(
    //   (maxWidthCol, col, i) => col.width >= columns[maxWidthCol].width ? i : maxWidthCol, 0
    // ),
    offset: 0,
    limit: 10,
    totalCount: -1,
    data: [],
    orderBy: {
      [columns[0].orderBy]: 'asc'
    }
  })

  let loading = true, error, data
  const result = useQuery(query, {
    variables: { offset: pageInfo.offset, limit: pageInfo.limit, orderBy: pageInfo.orderBy },
  });
  loading = result.loading
  data = result.data

  const isItemLoaded = index => !!pageInfo.data[index];

  const areItemsLoaded = (startIndex, endIndex) => {
    let allLoaded = true;
    for (var i=startIndex; i<=endIndex && allLoaded; ++i) {
      if (!isItemLoaded(i)) {
        allLoaded = false;
      }
    }

    return allLoaded;
  }

  const loadMoreItems = (startIndex, stopIndex) => {
    if (!areItemsLoaded(startIndex, stopIndex)) {
      setPageInfo({
        ...pageInfo,
        offset: startIndex,
        limit: stopIndex - startIndex + 1,
      })
    }
  }

  const listRef = useRef(null);

  const onOrderBy = orderByColumn => {
    let order = 'asc'
    if (pageInfo.orderBy[orderByColumn] === 'asc') {
      order = 'desc'
    }
    listRef.current._listRef.scrollToItem(0);
    setPageInfo({ ...pageInfo, data: [], orderBy: { [orderByColumn]: order } })
  }

  const getColStyle = (column, colIndex) => {
    const colStyle = {
      flexGrow: 0,
      flexShrink: 0,
      flexBasis: `${colWidthPct[colIndex]}%`,
      minWidth: column.width,
    }
    // const colStyle = {
    //   flexGrow: 0,
    //   flexShrink: 0,
    //   flexBasis: column.width,
    // }
    // if (pageInfo.growColNo === colIndex) {
    //   colStyle.flexGrow = 1
    // } else {
    //   colStyle.width = column.width
    // }
    return colStyle
  }

  const Row = props => {
    const { index, style } = props;
    const rowData = pageInfo.data[index]
    let label;
    if (!!rowData) {
      label = rowData.surname + ' ' + rowData.givenName;
    } else {
      label = "Loading...";
    }
    const onClick = () => { onRowClicked(rowData) }
    return (
      <div className={controlClass.row} style={style} onClick={onClick}>
        {columns.map((column, colIndex) => (
          <div key={colIndex} className={controlClass.column} style={getColStyle(column, colIndex)}>
            {
              rowData ?
                (column.view ?
                  column.view(column.value ? column.value(rowData) : rowData.id, colIndex) :
                  <DisplayData type={column.type} value={column.value(rowData)} />
                ) :
                <Skeleton width="60%" />
            }
          </div>
        ))}
      </div>
    );
  }

  if (!loading) {
    const { pageInfo: { totalCount }, nodes } = data.people

    if (pageInfo.totalCount === -1 && totalCount >= 0) {
      const newPageInfo = { ...pageInfo, totalCount }
      setPageInfo(newPageInfo);
    } else if (nodes.length > 0 && !isItemLoaded(pageInfo.offset)) {
      for (let index = pageInfo.offset; index <= pageInfo.offset + pageInfo.limit; index++) {
        pageInfo.data[index] = nodes[index - pageInfo.offset]
      }
      setPageInfo({
        data: pageInfo.data,
        ...pageInfo,
      })
      // var rowsThisPage = nodes.map(node => ({ name: node.surname + node.givenName, ...node }));
    }
  }

  // const onGridReady = params => {
  //   const gridApi = params.api;
  //   const gridColumnApi = params.columnApi;

  //   var dataSource = {
  //     rowCount: null,
  //     getRows: function(getRowParams) {
  //       console.log('asking for ' + getRowParams.startRow + ' to ' + getRowParams.endRow);

  //       setPageInfo({
  //         offset: getRowParams.startRow,
  //         limit: getRowParams.endRow - getRowParams.startRow,
  //         callback: getRowParams.successCallback
  //       })
  //     },
  //   };
  //   params.api.setDatasource(dataSource);
  // };

  const gridContainerRef = useRef(null)
  const listContainerRef = useRef(null)
  const headerRowContainerRef = useRef(null)
  const headerRowRef = useRef(null)

  // useEffect(() => {
  //   console.log('didmount')
  //   console.log(listContainerRef)
    // if (listContainerRef.current) {
    //   console.log(listContainerRef.current.scrollLeft)
    //   headerRowRef.current.scrollLeft = listContainerRef.current.scrollLeft
  //     const scrollBarWidth = gridContainerRef.current.offsetWidth - listContainerRef.current.clientWidth
  //     console.log(gridContainerRef.current.offsetWidth)
  //     console.log(listContainerRef.current.clientWidth)
  //     headerRowRef.current.marginRight = 5
  //   }
  // });

  const onHoriScroll = e => {
    let element = e.target
// console.log('scrollLeft')
// console.log(element.className)
// console.log(listRef.current)
    if (element.className === 'List') {
      headerRowRef.current.scrollLeft = element.scrollLeft
    }
    if (!headerRowContainerRef.current.style.paddingRight) {
      const scrollbarWidth = gridContainerRef.current.offsetWidth - element.clientWidth
      headerRowContainerRef.current.style.paddingRight = `${scrollbarWidth}px`
    }
  }

  return (
    <SectionCardContainer>
      <SectionCard>
        <div ref={gridContainerRef}>
          <HeaderRow columns={columns} getColStyle={getColStyle} orderBy={pageInfo.orderBy} onOrderBy={onOrderBy} headerRowContainerRef={headerRowContainerRef} headerRowRef={headerRowRef} />
          <div style={{ width: '100%', height: 'calc(100vh - 260px)', marginTop: 0, overflowX: 'auto', scrollbarWidth: 10 }} onScroll={onHoriScroll} ref={listContainerRef}>
            <AutoSizer>
              {({ height, width }) => (
                <InfiniteLoader
                  isItemLoaded={isItemLoaded}
                  itemCount={pageInfo.totalCount}
                  loadMoreItems={loadMoreItems}
                  ref={listRef}
                >
                  {({ onItemsRendered, ref }) => (
                    <List
                      className="List"
                      width={width}
                      height={height}
                      itemCount={pageInfo.totalCount >= 0 ? pageInfo.totalCount : 10}
                      itemSize={42}
                      onItemsRendered={onItemsRendered}
                      ref={ref}
                    >
                      {Row}
                    </List>
                  )}
                </InfiniteLoader>
              )}
            </AutoSizer>
          </div>
          {pageInfo.totalCount >= 0 && <div className={controlClass.rowCount}>共 {pageInfo.totalCount} 個紀錄</div>}
        </div>
      </SectionCard>
    </SectionCardContainer>
  );
}
