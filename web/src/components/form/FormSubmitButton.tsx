import * as React from 'react'
import { OnSubmit } from "react-hook-form";
import { FormContext } from './Form';
import {
  PrimaryButton,
  mergeStyles,
} from '@fluentui/react';

const buttonClass = mergeStyles({
  width: 'auto'
})

interface IProps {
  text: string,
  onSubmit: OnSubmit<Record<string, any>>
}

export const FormSubmitButton: React.SFC<IProps> = ({ text, onSubmit })=> {
  const { handleSubmit } = React.useContext(FormContext)

  return (
    <PrimaryButton text={text} className={buttonClass} onClick={handleSubmit(onSubmit)} />
  )
}
