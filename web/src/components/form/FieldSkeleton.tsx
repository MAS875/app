import * as React from 'react'
import Skeleton from "react-loading-skeleton"

interface IProps {
  loading?: boolean,
}

export const FieldSkeleton: React.SFC<IProps> = (props) => {
  const { loading, children } = props;
  return loading ? (
    <Skeleton width={100} />
  ) : (
    <>
      {children}
    </>
  )
}
