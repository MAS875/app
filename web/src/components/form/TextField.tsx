import * as React from 'react'
import { Controller, ValidationOptions } from "react-hook-form";
import { FormContext, getErrorMessage } from './Form';
import {
  TextField as FluentTextField,
  Label,
} from '@fluentui/react';
import { FieldSkeleton } from './FieldSkeleton'

interface ITextFieldProps {
  readonly?: boolean,
  loading?: boolean,
  label?: string,
  placeholder?: string
  errorMessage?: string,
  required?: boolean,
  onChange: any,
  onBlur: any,
  value?: string,
}

const MyTextField: React.SFC<ITextFieldProps> = (props) => {
  const { readonly, loading, label, value, required, ...forwardProps } = props

  return !readonly ? (
    <FluentTextField
      {...forwardProps}
      value={value}
      label={label}
      required={label && required}
    />
  ) : (
    <div>
      {label && <Label>{label}</Label>}
      <div>
        <FieldSkeleton loading={loading}>
          {value}
        </FieldSkeleton>
      </div>
    </div>
  )
}

interface IProps {
  name: string,
  label?: string,
  placeholder?: string,
  defaultValue?: string,
  validate?: ValidationOptions,
  required?: boolean,
}

export const TextField: React.SFC<IProps> = (props) => {
  const { name, validate, required, ...forwardProps } = props
  const { readonly, loading, control, errors } = React.useContext(FormContext)
  const mergedProps = {
    readonly,
    loading,
    ...forwardProps,
  }
  const rules = validate || {}
  rules.required = required

  return (
    <Controller
      name={name}
      control={control}
      as={MyTextField}
      rules={rules}
      required={required}
      errorMessage={getErrorMessage(errors, name, rules)}
      {...mergedProps}
    />
  )
}
