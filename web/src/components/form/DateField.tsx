import * as React from 'react'
import { Controller, ValidationOptions } from "react-hook-form";
import { FormContext, Form } from './Form';
import {
  Label,
  DatePicker,
  mergeStyleSets,
  DayOfWeek,
  IDatePickerStrings,
} from '@fluentui/react';
import { ErrorMessage } from './ErrorMessage'
import { FieldSkeleton } from './FieldSkeleton'

const controlClass = mergeStyleSets({
  control: {
    margin: '0 0 0 0',
    marginTop: '0px !important',
    maxWidth: '150px',
  },
});

const firstDayOfWeek = DayOfWeek.Sunday;

const DayPickerStrings: IDatePickerStrings = {
  months: [
    '一月',
    '二月',
    '三月',
    '四月',
    '五月',
    '六月',
    '七月',
    '八月',
    '九月',
    '十月',
    '十一月',
    '十二月',
  ],

  shortMonths: [
    '一月',
    '二月',
    '三月',
    '四月',
    '五月',
    '六月',
    '七月',
    '八月',
    '九月',
    '十月',
    '十一月',
    '十二月',
  ],

  days: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],

  shortDays: ['日', '一', '二', '三', '四', '五', '六'],

  goToToday: '今日',
  prevMonthAriaLabel: '上一個月',
  nextMonthAriaLabel: '下一個月',
  prevYearAriaLabel: '上一年',
  nextYearAriaLabel: '下一年',
  closeButtonAriaLabel: '關閉',

  isRequiredErrorMessage: '必須提供',

  invalidInputErrorMessage: '日期格式錯誤',
};

const onFormatDate = (date: Date): string => {
  if (date) {
    return date.getFullYear() + '-' +
      (date.getMonth() + 1).toString().padStart(2, '0') + '-' +
      date.getDate().toString().padStart(2, '0');
  } else {
    return '';
  }
};

const onParseDateFromString = (val: string): Date => {
  if (val) {
    const dateParts = val.split('-');
    return new Date(parseInt(dateParts[0]), parseInt(dateParts[1])-1, parseInt(dateParts[2]));
  }
}

interface IDatePickerProps {
  loading?: boolean,
  readonly?: boolean,
  label?: string,
  required?: boolean,
  onChange: any,
  onBlur: any,
  value?: Date,
}

const MyDatePicker: React.SFC<IDatePickerProps> = (props) => {
  const { loading, readonly, label, value, onChange, onBlur, ...forwardProps } = props;

  if (!readonly) {
    const onSelectDate = (date: Date) => {
      onChange(date.toISOString());
      onBlur();
    }

    return (
      <DatePicker
        {...forwardProps}
        label={label}
        value={value && new Date(value)}
        className={controlClass.control}
        showMonthPickerAsOverlay={false}
        allowTextInput={true}
        firstDayOfWeek={firstDayOfWeek}
        strings={DayPickerStrings}
        formatDate={onFormatDate}
        parseDateFromString={onParseDateFromString}
        onSelectDate={onSelectDate}
      />
    )
  } else {
    return (
      <FieldSkeleton loading={loading}>
        {onFormatDate(value && new Date(value))}
      </FieldSkeleton>
    )
  }
}

interface IProps {
  readonly?: boolean,
  loading?: boolean,
  name: string,
  label?: string,
  defaultValue?: string,
  validate?: ValidationOptions,
  required?: boolean,
}

export const DateField: React.SFC<IProps> = (props) => {
  const { name, label, validate, required, ...forwardProps } = props;
  const { readonly, loading, control, errors } = React.useContext(FormContext)
  const mergedProps = {
    readonly,
    loading,
    ...forwardProps,
  }
  const rules = validate || {};
  rules.required = required;

  return (
    <>
      <Label required={required}>{label}</Label>
      <Controller
        name={name}
        control={control}
        as={MyDatePicker}
        rules={rules}
        {...mergedProps}
      />
      <ErrorMessage name={name} errors={errors} rules={rules} />
    </>
  );
}
