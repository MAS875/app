import * as React from 'react'
import {
  Stack,
  mergeStyles,
} from '@fluentui/react';
import { SectionCard } from 'src/components/SectionCard';

interface IProps {
}

export const ActionContainer: React.SFC<IProps> = ({ children }) => {
  return (
    <SectionCard style={{ flex: '1 1 100%', paddingTop: 20, paddingBottom: 20 }}>
      <Stack horizontal horizontalAlign="end">
        {children}
      </Stack>
    </SectionCard>
  )
}
