import * as React from 'react'
import { Controller, ValidationOptions } from "react-hook-form";
import {
  Label,
  Stack,
  DefaultButton,
  PrimaryButton,
  IChoiceGroupOption,
} from '@fluentui/react';
import { FormContext } from './Form';
import { ErrorMessage } from './ErrorMessage';

interface IButtonGroupViewProps {
  readonly?: boolean
  label?: string,
  required?: boolean,
  options: IChoiceGroupOption[],
  onChange: any,
  onBlur: any,
  value?: string,
}

const ButtonGroupView: React.SFC<IButtonGroupViewProps> = (props) => {
  const { readonly, options, onChange, onBlur, value, ...forwardProps } = props

  if (!readonly) {
    const onKeyChange = (ev, option) => {
      onChange(option.key);
      onBlur();
    }

    return (
      <Stack horizontal>
        {options.map(option => (
          value === option.key ?
            <PrimaryButton key={option.key} text={option.text} /> :
            <DefaultButton key={option.key} text={option.text} onClick={ev => onKeyChange(ev, option)} />
        ))}
      </Stack>
    )
  } else {
    const selectedOption = options.filter(option => option.key === value)
    return (
      <div>{selectedOption.length > 0 ? selectedOption[0].text : ""}</div>
    )
  }
}

interface IProps {
  name: string,
  label?: string,
  defaultValue?: string,
  validate?: ValidationOptions,
  required?: boolean,
  options: IChoiceGroupOption[],
  horizontal?: boolean
}

export const ButtonGroupField: React.SFC<IProps> = (props) => {
  const { label, name, validate, required, ...forwardProps } = props;
  const { readonly, loading, control, errors } = React.useContext(FormContext);
  const mergedProps = {
    readonly,
    loading,
    ...forwardProps,
  }
  const rules = validate || {};
  rules.required = required;

  return (
    <>
      {label && <Label required={required}>{label}</Label>}
      <Controller
        name={name}
        control={control}
        as={ButtonGroupView}
        rules={rules}
        required={required}
        {...mergedProps}
      />
      <ErrorMessage errors={errors} name={name} rules={rules} />
    </>
  );
}
