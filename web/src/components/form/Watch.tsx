import * as React from 'react'
import { FormContext } from './Form';

type IProps = {
  names?: string[],
  name?: string,
  defaultValue: any,
  defaultValues?: object,
  children: any,
}

export const Watch = (props: IProps) => {
  const { names, name, defaultValues, defaultValue, children } = props
  const { watch } = React.useContext(FormContext)

  if (names) {
    const watchFields = watch(names, defaultValues);
    return children(watchFields)
  } else {
    const watchField = watch(name, defaultValue);
    return children(watchField)
  }
}
