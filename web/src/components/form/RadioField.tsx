import * as React from 'react'
import { Controller, ValidationOptions } from "react-hook-form";
import { FormContext } from './Form';
import { ErrorMessage } from './ErrorMessage';
import {
  ChoiceGroup,
  Label,
  IChoiceGroupOption,
  IChoiceGroupOptionStyles,
} from '@fluentui/react';

interface IChoiceGroupProps {
  readonly?: boolean,
  label?: string,
  required?: boolean,
  horizontal?: boolean,
  options: IChoiceGroupOption[],
  onChange: any,
  onBlur: any,
  value?: string,
}

const MyChoiceGroup: React.SFC<IChoiceGroupProps> = (props) => {
  const { readonly, label, required, horizontal, options, onChange, onBlur, value, ...forwardProps } = props;

  if (!readonly) {
    let optionsWithStyles = options;
    if (horizontal) {
      const optionStyles: IChoiceGroupOptionStyles = {
        root: {
          display: 'inline-block',
          marginRight: 20
        },
      }
      optionsWithStyles = options.map(option => ({ ...option, styles: optionStyles }))
    }

    const selectedKey = value;
    const onKeyChange = (ev, option) => {
      onChange(option.key);
      onBlur();
    }

    return (
      <ChoiceGroup
        {...forwardProps}
        label={label}
        required={label && required}
        options={optionsWithStyles}
        selectedKey={selectedKey}
        onChange={onKeyChange}
      />
    )
  } else {
    const selectedOption = options.filter(option => option.key === value)
    return (
      <div>
        {label && <Label>{label}</Label>}
        <div>{selectedOption.length > 0 ? selectedOption[0].text : ""}</div>
      </div>
    )
  }
}

interface IProps {
  name: string,
  label?: string,
  defaultValue?: string,
  validate?: ValidationOptions,
  required?: boolean,
  options: IChoiceGroupOption[],
  horizontal?: boolean
}

export const RadioField: React.SFC<IProps> = (props) => {
  const { name, validate, required, ...forwardProps } = props;
  const { readonly, loading, control, errors } = React.useContext(FormContext);
  const mergedProps = {
    readonly,
    loading,
    ...forwardProps,
  }
  const rules = validate || {};
  rules.required = required;

  return (
    <>
      <Controller
        name={name}
        control={control}
        as={MyChoiceGroup}
        rules={rules}
        required={required}
        {...mergedProps}
      />
      <ErrorMessage errors={errors} name={name} rules={rules} />
    </>
  );
}
