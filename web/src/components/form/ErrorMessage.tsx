import * as React from 'react'
import {
  mergeStyleSets,
} from '@fluentui/react';
import { getErrorMessage } from './Form';

const controlClass = mergeStyleSets({
  alert: {
    marginTop: '0 !important',
  },
  errorMessage: {
    animationName: 'css-1, css-14',
    animationDuration: '0.367s',
    animationTimingFunction: 'cubic-bezier(0.1, 0.9, 0.2, 1)',
    animationFillMode: 'both',
    fontSize: '12px',
    color: 'rgb(164, 38, 44)',
    marginTop: '0px',
    marginRight: '0px',
    marginBottom: '0px',
    marginLeft: '0px',
    paddingTop: '5px',
    display: 'flex',
    alignItems: 'center',
  },
});

interface IProps {
  errors: object,
  name: string,
  rules: object
}

export const ErrorMessage: React.SFC<IProps> = ({ errors, name, rules }) => {
  const errorMessage = getErrorMessage(errors, name, rules);

  return (
    <>
      {errorMessage &&
        <div role="alert" className={controlClass.alert}>
          <p className={controlClass.errorMessage}>
            <span data-automation-id="error-message">
              {errorMessage}
            </span>
          </p>
        </div>
      }
    </>
  )
}
