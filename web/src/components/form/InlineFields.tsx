import * as React from 'react'
import {
  Label,
  Stack,
} from '@fluentui/react';
import { FormContext } from './Form';

const stackTokens = { childrenGap: 15 };

interface IProps {
  required?: boolean,
  label?: string,
}

export const InlineFields: React.SFC<IProps> = ({ required, label, children }) => {
  const { readonly } = React.useContext(FormContext)
  return (
    <div>
      <Label required={!readonly && required}>{label}</Label>
      <Stack horizontal tokens={stackTokens}>
          {children}
      </Stack>
    </div>
  )
}