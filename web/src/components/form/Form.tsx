import * as React from 'react'
import { useForm, Control, OnSubmit } from "react-hook-form";

const defaultErrorMessage = {
  required: () => '必須提供',
  maxLength: len => `最多${len}個字`,
}

interface IContextProps {
  readonly: boolean,
  loading: boolean,
  errors: object,
  control: Control<Record<string, any>>,
  handleSubmit: any,
  watch: any,
}

export const FormContext = React.createContext({ readonly: false } as IContextProps);

export const getErrorMessage = (errors, field, validate) => {
  let message: string

  if (errors && errors[field]) {
    const error = errors[field]
    const errorParam = validate[error.type]
    message = error.message
    if (message === '') {
      message = defaultErrorMessage[error.type](errorParam) || '輸入不正確'
    }
  }
  return message
}

// export const prepareDataForSubmit = (value: object): object => {
//   Object.keys(value).forEach((key: any) => {
//     if (value[key] instanceof Date) {
//       value[key] = value[key].toISOString();
//     } else if (Array.isArray(value[key])) {
//       value[key] = value[key].map(item => prepareDataForSubmit(item))
//     } else if (typeof(value[key]) === 'object') {
//       value[key] = prepareDataForSubmit(value[key])
//     }
//   })

//   return value;
// }


interface IProps {
  readonly?: boolean,
  loading?: boolean,
  defaultValues?: object,
  onSubmit?: OnSubmit<Record<string, any>>,
  children: any,
}

export const Form: React.SFC<IProps> = ({ readonly, loading, defaultValues, onSubmit, children }) => {
  const methods = useForm({
    defaultValues,
    mode: 'onBlur',
  })

  // ref: https://github.com/react-hook-form/react-hook-form/issues/1042
  React.useEffect(() => {
    if (defaultValues) {
      methods.reset(defaultValues)
    }
  }, [defaultValues])

  const formContext = {
    readonly,
    loading,
    ...methods
  }

  return (
    <FormContext.Provider value={formContext}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        {typeof children === 'function' ? children(methods) : children}
      </form>
    </FormContext.Provider>
  )
}
