import * as React from 'react';
import {
  Stack,
  IStackProps,
  IStackTokens,
  IStyle,
  mergeStyleSets,
  mergeStyles,
} from '@fluentui/react';

const sectionStyle:IStyle = {
  position: 'relative',
  background: '#fff',
  padding: '28px',
  // marginBottom: '28px',
  borderRadius: '2px',
  backgroundClip: 'padding-box',
  boxShadow: '0 1.6px 3.6px 0 rgba(0,0,0,0.132), 0 0.3px 0.9px 0 rgba(0,0,0,0.108)',
  transitionDelay: '0s',
  opacity: 1,
  transform: 'translate3d(0, 0, 0)',
  transition: 'transform 300ms cubic-bezier(0.1, 0.9, 0.2, 1),opacity 300ms cubic-bezier(0.1, 0.9, 0.2, 1) 0.05s,-webkit-transform 300ms cubic-bezier(0.1, 0.9, 0.2, 1)',
  flex: '1 1 300px',
  minWidth: 0,  // to avoid overflow
}

const defaultStyles = mergeStyleSets({
  header: {
    marginBottom: '20px',
    display: 'flex',
    // '-webkitBoxPack': 'justify',
    justifyContent: 'space-between',
    // '-webkitBoxAlign': 'center',
    alignItems: 'center',
  },
  subheading: {
    fontSize: '20px',
    outline: 'none',
    color: '#323130',
    fontWeight: 600,
    lineHeight: '1',
    marginTop: 0,
    marginBottom: 0,
  },
  sectionCardContainer: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center'
  },
});

const columnProps: Partial<IStackProps> = {
  tokens: { childrenGap: 15 },
  // styles: { root: { width: 300 } },
};

interface IProps {
  title?: string,
  style?: IStyle,
}

export const SectionCard: React.SFC<IProps> = ({ title, style, children }) => {
  let sectionClassName = mergeStyles(sectionStyle, style)

  return (
    <div className={sectionClassName}>
      {title &&
        <div className={defaultStyles.header}>
          <h2 className={defaultStyles.subheading}>
            {title}
          </h2>
        </div>
      }
      <div>
          <div>
            <Stack {...columnProps}>
              {children}
            </Stack>
          </div>
      </div>
    </div>
  );
}

interface IContainerProps {
}

const sectionStackTokens: IStackTokens = { childrenGap: 30 };

export const SectionCardContainer: React.SFC<IContainerProps> = ({ children }) => (
  <Stack horizontal wrap tokens={sectionStackTokens}>
    {children}
  </Stack>
)
