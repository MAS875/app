import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Stack, IStackTokens, IStyle, mergeStyleSets, mergeStyles } from '@fluentui/react';
import LogoComponent from './LogoComponent';
import MenuItemComponent from './MenuItemComponent';
import { SidebarMenu } from './SidebarMenu';
import IconOverview from '../../assets/icon-burger.js';
import IconTickets from '../../assets/icon-burger.js';
import IconIdeas from '../../assets/icon-burger.js';
import IconContacts from '../../assets/icon-burger';
import IconAgents from '../../assets/icon-burger';
import IconArticles from '../../assets/icon-burger';
import IconSettings from '../../assets/icon-burger';
import IconSubscription from '../../assets/icon-burger';
import IconBurger from '../../assets/icon-burger';

const styles = StyleSheet.create({
    burgerIcon: {
        cursor: 'pointer',
        position: 'absolute',
        left: 24,
        top: 34
    },
    mainContainerMobile: {
        position: 'absolute',
        width: '100vw',
        minWidth: '100%',
        top: 0,
        left: 0
    },
    outsideLayer: {
        position: 'absolute',
        width: '100vw',
        minWidth: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,.50)',
        zIndex: 900
    },
    separator: {
        borderTop: '1px solid #DFE0EB',
        marginTop: 16,
        marginBottom: 16,
        opacity: 0.06
    },
});

const controlClass = mergeStyleSets({
    mainContainer: {
        height: '100%',
        minHeight: '100vh',
        selectors: {
            '@media(max-width: 768px)': {
                position: 'absolute',
                width: '100vw',
                minWidth: '100%',
                top: 0,
                left: 0
            }
        }
    },
    menuItemList: {
        marginTop: 28
    },
})

const containerStyles:IStyle = {
    backgroundColor: '#FFFFFF',
    width: 255,
    paddingTop: 34,
    height: 'calc(100% - 32px)',
    selectors: {
        '@media(max-width: 768px)': {
            transition: 'left 0.5s, right 0.5s',
            position: 'absolute',
            width: 255,
            height: 'calc(100% - 32px)',
            zIndex: 901
        }
    }
}

const containerHideStyles = {
    left: -255
}

const containerShowStyles = {
    left: 0
}

const colToken:IStackTokens = {
    childrenGap: 28,
}

interface IState {
    expanded: Boolean;
}

interface IProps {
    activeItem: string;
}

class SidebarComponent extends React.Component<IProps> {

    state = { expanded: false };

    // onItemClicked = (item) => {
    //     this.setState({ expanded: false });
    //     return this.props.onChange(item);
    // }

    isMobile = () => window.innerWidth <= 768;

    toggleMenu = () => this.setState((prevState: IState) => ({ expanded: !prevState.expanded }));

    renderBurger = () => {
        return <div onClick={this.toggleMenu} className={css(styles.burgerIcon)}>
            <IconBurger />
        </div>
    }

    render() {
        const { expanded } = this.state;
        const { activeItem } = this.props;
        const isMobile = this.isMobile();
        const containerClass = expanded ?
            mergeStyles(containerStyles, containerShowStyles) :
            mergeStyles(containerStyles, containerHideStyles)
        return (
            <div style={{ position: 'relative' }}>
                <Stack horizontal className={controlClass.mainContainer}>
                    {(isMobile && !expanded) && this.renderBurger()}
                    <Stack.Item>
                        <Stack className={containerClass} verticalFill={true} tokens={colToken}>
                            <Stack.Item><LogoComponent /></Stack.Item>
                            <Stack.Item grow={1} className={controlClass.menuItemList}>
                                <SidebarMenu activeItem={activeItem} />
                                {/* <MenuItemComponent
                                    title="Overview" icon={IconOverview}
                                    onClick={() => this.onItemClicked('Overview')}
                                    active={this.props.selectedItem === 'Overview'}
                                />
                                <MenuItemComponent
                                    title="Tickets" icon={IconTickets}
                                    onClick={() => this.onItemClicked('Tickets')}
                                    active={this.props.selectedItem === 'Tickets'}
                                />
                                <MenuItemComponent
                                    title="Ideas" icon={IconIdeas}
                                    onClick={() => this.onItemClicked('Ideas')}
                                    active={this.props.selectedItem === 'Ideas'} />
                                <MenuItemComponent
                                    title="Contacts" icon={IconContacts}
                                    onClick={() => this.onItemClicked('Contacts')}
                                    active={this.props.selectedItem === 'Contacts'} />
                                <MenuItemComponent
                                    title="Agents" icon={IconAgents}
                                    onClick={() => this.onItemClicked('Agents')}
                                    active={this.props.selectedItem === 'Agents'} />
                                <MenuItemComponent
                                    title="Articles" icon={IconArticles}
                                    onClick={() => this.onItemClicked('Articles')}
                                    active={this.props.selectedItem === 'Articles'} />
                                <div className={css(styles.separator)}></div>
                                <MenuItemComponent
                                    title="Settings" icon={IconSettings}
                                    onClick={() => this.onItemClicked('Settings')}
                                    active={this.props.selectedItem === 'Settings'} />
                                <MenuItemComponent
                                    title="Subscription" icon={IconSubscription}
                                    onClick={() => this.onItemClicked('Subscription')}
                                    active={this.props.selectedItem === 'Subscription'} /> */}
                            </Stack.Item>
                        </Stack>
                    </Stack.Item>
                    {isMobile && expanded && <Stack.Item grow={1}><div className={css(styles.outsideLayer)} onClick={this.toggleMenu}></div></Stack.Item>}
                </Stack>
            </div>
        );
    };
}

export default SidebarComponent;