import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Stack } from '@fluentui/react';
// import { Link, routes } from '@redwoodjs/router'
import {
    Link
} from "react-router-dom";
import Logo from '../../assets/icon-logo';

const styles = StyleSheet.create({
    title: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 19,
        lineHeight: '24px',
        letterSpacing: '0.4px',
        color: 'rgba(0,0,0,.9)',
        opacity: 0.7,
        marginLeft: 12,
    }
});

const containerStyles = {
    root: {
        marginLeft: 32,
        marginRight: 32,
        alignItems: 'center'
    },
}

function LogoComponent() {
    return (
        <Link to="/">
            <Stack horizontal horizontalAlign="center" styles={containerStyles}>
                <Stack.Item align="center">
                    <Logo />
                </Stack.Item>
                <Stack.Item align="center">
                    <span className={css(styles.title)}>教會管理系統</span>
                </Stack.Item>
            </Stack>
        </Link>
    );
}

export default LogoComponent;