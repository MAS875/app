import React from 'react';
import { bool, func, string } from 'prop-types';
import { StyleSheet, css } from 'aphrodite';
import { Stack } from '@fluentui/react';

const styles = StyleSheet.create({
    activeBar: {
        height: 56,
        width: 3,
        backgroundColor: '#DDE2FF',
        position: 'absolute',
        left: 0
    },
    activeTitle: {
        color: '#DDE2FF'
    },
    title: {
        fontSize: 16,
        lineHeight: '20px',
        letterSpacing: '0.2px',
        color: '#A4A6B3',
        marginLeft: 24
    }
});

const containerStyles = {
    height: 56,
    cursor: 'pointer',
    ':hover': {
        backgroundColor: 'rgba(221,226,255, 0.08)'
    },
    paddingLeft: 32,
    paddingRight: 32
}

const activeContainerStyles = {
    backgroundColor: 'rgba(221,226,255, 0.08)'
}

function MenuItemComponent(props) {
    const { active, icon, title, ...otherProps } = props;
    const Icon = icon;
    let containerClassName = !active ?
        mergeStyles(containerStyles) :
        mergeStyles(containerStyles, activeContainerStyles)

    return (
        <Stack horizontal horizontalAlign="center" className={containerClassName} {...otherProps}>
            {active && <Stack.Item align="center"><div className={css(styles.activeBar)}></div></Stack.Item>}
            <Stack.Item align="center">
                <Icon fill={active && "#DDE2FF"} opacity={!active && "0.4"} />
            </Stack.Item>
            <Stack.Item align="center">
                <span className={css(styles.title, active && styles.activeTitle)}>{title}</span>
            </Stack.Item>
        </Stack>
    );
}

MenuItemComponent.propTypes = {
    active: bool,
    icon: func,
    title: string
};

export default MenuItemComponent;