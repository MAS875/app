import * as React from 'react';
import { Nav, INavLink, INavStyles, INavLinkGroup, INavButtonProps } from '@fluentui/react';
//import { navigate, routes } from '@redwoodjs/router'
import { useHistory } from "react-router-dom";
import SideMenu from 'react-sidemenu';
import 'react-sidemenu/dist/side-menu.css'

const items = [
  {divider: true, label: '會友', value: 'peopleDivider'},
  {label: '會友名冊', value: '/people', icon: 'fa-search'},
  // {label: '搜尋會友', value: '/people/search', icon: 'fa-search'},
  {label: '新增會友', value: '/people/new', icon: 'far fa-plus-square'},
  {divider: true, label: '聚會', value: 'gatheringDivider'},
  // {label: '行事曆', value: 'home', icon: 'far fa-calendar'}
];

interface SidebarMenuProps {
  activeItem: string,
 }

export const SidebarMenu: React.SFC<SidebarMenuProps> = ({ activeItem }) => {
  let history = useHistory();

  const onMenuItemClick = value => {
    history.push(value);
  }

  return (
    <SideMenu
      theme="churchadmin"
      items={items}
      onMenuItemClick={onMenuItemClick}
      activeItem={activeItem}
     />
  );
};
