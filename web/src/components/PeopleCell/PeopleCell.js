import { Link, routes } from '@redwoodjs/router'

import People from 'src/components/People'

export const QUERY = gql`
  query PEOPLE($orderBy: PersonOrderByInput, $offset: Int!, $limit: Int!) {
    people(orderBy: $orderBy, offset: $offset, limit: $limit) {
      id
      surname
      givenName
      gender
      createdAt
    }
  }
`

export const beforeQuery = (props) => {
  return { variables: props, fetchPolicy: 'cache-and-network' }
}

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="text-center">
      {'No people yet. '}
      <Link
        to={routes.newPerson()}
        className="text-blue-500 underline hover:text-blue-700"
      >
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Success = ({ people }) => {
  return <People people={people} />
}
