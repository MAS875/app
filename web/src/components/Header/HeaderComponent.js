import React from 'react';
import { string } from 'prop-types';
import { StyleSheet, css } from 'aphrodite';
import { Stack } from '@fluentui/react';
import IconSearch from '../../assets/icon-search';
import IconBellNew from '../../assets/icon-bell-new';

const styles = StyleSheet.create({
    avatar: {
        height: 35,
        width: 35,
        borderRadius: 50,
        marginLeft: 14,
        border: '1px solid #DFE0EB',
    },
    cursorPointer: {
        cursor: 'pointer'
    },
    name: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 14,
        lineHeight: '20px',
        textAlign: 'right',
        letterSpacing: 0.2,
        '@media (max-width: 768px)': {
            display: 'none'
        }
    },
    separator: {
        borderLeft: '1px solid #DFE0EB',
        marginLeft: 32,
        marginRight: 32,
        height: 32,
        width: 2,
        '@media (max-width: 768px)': {
            marginLeft: 12,
            marginRight: 12
        }
    },
    iconStyles: {
        cursor: 'pointer',
        marginLeft: 25,
        '@media (max-width: 768px)': {
            marginLeft: 12
        }
    }
});

const containerStyles = {
    root: {
        height: 40,
    },
}

const titleStyles = {
    root: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: '30px',
        letterSpacing: 0.3,
        selectors: {
            '@media (max-width: 768px)': {
                marginLeft: 36
            },
            '@media (max-width: 468px)': {
                fontSize: 20
            }
        }
    },
}

function HeaderComponent(props) {
    const { icon, title, ...otherProps } = props;
    return (
        <Stack horizontal styles={containerStyles} {...otherProps}>
            <Stack.Item align="center" styles={titleStyles}>{title}</Stack.Item>
            <Stack.Item align="center" grow={1}>
                <Stack horizontal horizontalAlign="end">
                    <Stack.Item align="center">
                        <div className={css(styles.iconStyles)}>
                            <IconSearch />
                        </div>
                    </Stack.Item>
                    <Stack.Item align="center">
                        <div className={css(styles.iconStyles)}>
                            <IconBellNew />
                        </div>
                    </Stack.Item>
                    <Stack.Item align="center">
                        <div className={css(styles.separator)}></div>
                    </Stack.Item>
                    <Stack.Item align="center">
                        <Stack horizontal>
                            <Stack.Item align="center">
                                <span className={css(styles.name, styles.cursorPointer)}>Germán Llorente</span>
                            </Stack.Item>
                            <Stack.Item align="center">
                                <img src="https://avatars3.githubusercontent.com/u/21162888?s=460&v=4" alt="avatar" className={css(styles.avatar, styles.cursorPointer)} />
                            </Stack.Item>
                        </Stack>
                    </Stack.Item>
                </Stack>
            </Stack.Item>
        </Stack>
    );
}

HeaderComponent.propTypes = {
    title: string
};

export default HeaderComponent;