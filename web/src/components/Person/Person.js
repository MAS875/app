import { useQuery, useMutation } from '@redwoodjs/web'
// import { Link, routes, navigate } from '@redwoodjs/router'
import { useHistory } from "react-router-dom";
import PersonForm from 'src/components/PersonForm'

export const QUERY = gql`
  query FIND_PERSON_BY_ID($id: String!) {
    person: person(id: $id) {
      id
      surname
      givenName
      surnameEn
      givenNameEn
      gender
      phone
      phone2
      idCardNo
      email
      birthday
      firstVisitOn
      joinChurchOn
      baptismOn
      baptismChurch
      createdAt
      updatedAt

    }
  }
`

const DELETE_PERSON_MUTATION = gql`
  mutation DeletePersonMutation($id: String!) {
    deletePerson(id: $id) {
      id
    }
  }
`

const Person = ({ id }) => {
  const { loading, data } = useQuery(QUERY, { variables: {
    id,
  }});

  const [deletePerson] = useMutation(DELETE_PERSON_MUTATION, {
    onCompleted: () => {
      navigate(routes.people())
      location.reload()
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete person ' + id + '?')) {
      deletePerson({ variables: { id } })
    }
  }

  const history = useHistory();
  const onChangeClick = (id) => {
    history.push(`/people/${id}/edit`)
  }

  return (
    <PersonForm readonly loading={loading} person={data?.person} onChangeClick={onChangeClick} onDeleteClick={onDeleteClick} />
  )
}

export default Person
