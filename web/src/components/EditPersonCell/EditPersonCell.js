import { useMutation } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import PersonForm from 'src/components/PersonForm'

export const QUERY = gql`
  query FIND_PERSON_BY_ID($id: String!) {
    person: person(id: $id) {
      id
      surname
      givenName
      surnameEn
      givenNameEn
      gender
      phone
      phone2
      idCardNo
      email
      birthday
      firstVisitOn
      joinChurchOn
      baptismOn
      baptismChurch
      createdAt
      updatedAt
    }
  }
`
const UPDATE_PERSON_MUTATION = gql`
  mutation UpdatePersonMutation($id: String!, $input: UpdatePersonInput!) {
    updatePerson(id: $id, input: $input) {
      id
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Success = ({ person }) => {
  const [updatePerson, { loading, error }] = useMutation(UPDATE_PERSON_MUTATION, {
    onCompleted: () => {
      navigate(routes.people())
    },
  })

  const onSave = (input, id) => {
    updatePerson({ variables: { id, input } })
  }

  return (
    <div className="bg-white border rounded-lg overflow-hidden">
      <div className="bg-gray-100 p-4">
        <PersonForm person={person} onSave={onSave} error={error} loading={loading} />
      </div>
    </div>
  )
}
