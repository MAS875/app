import { useMutation } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import PersonForm from 'src/components/PersonForm'

const CREATE_PERSON_MUTATION = gql`
  mutation CreatePersonMutation($input: CreatePersonInput!) {
    createPerson(input: $input) {
      id
    }
  }
`

const NewPerson = () => {
  const [createPerson, { loading, error }] = useMutation(CREATE_PERSON_MUTATION, {
    onCompleted: () => {
      navigate(routes.people())
    },
  })

  const onSave = (input) => {
    createPerson({ variables: { input } })
  }

  return (
    <div className="bg-white border rounded-lg overflow-hidden">
      <div className="bg-gray-100 p-4">
        <PersonForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewPerson
