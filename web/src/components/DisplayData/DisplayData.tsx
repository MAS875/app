import * as React from 'react'

const display = {
  gender: data => data ? {
    Male: '男',
    Female: '女',
  }[data] : '',
  date: data => {
    if (data === null) {
      return ''
    } else {
      const date = new Date(data)
      return date.getFullYear() + "年" + (date.getMonth() + 1) + "月" + date.getDate() + '日'
    }
  },
  christianStatus: data => {
    if (data === true) {
      return '基督徒'
    } else if (data === false) {
      return '慕道者'
    } else {
      return '不清楚'
    }
  }
}

interface IProps {
  type: string,
  value: any,
}

export const displayData = (value, type) => {
  if (type && display[type]) {
    return display[type](value)
  } else if (value === null) {
    return ''
  } else if (typeof value === 'boolean') {
    return value ? '是' : '否'
  } else if (typeof value === 'string') {
    return value ? value : ''
  } else {
    return value
  }
}

export const DisplayData: React.SFC<IProps> = ({ type, value }) => displayData(value, type)
