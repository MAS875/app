import React, { useEffect, useState } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Stack } from '@fluentui/react';
import SidebarComponent from 'src/components/Sidebar/SidebarComponent';
import HeaderComponent from 'src/components/Header/HeaderComponent';

const styles = StyleSheet.create({
  content: {
      marginTop: 30
  },
});

const rowStackStyles = {
  root: {
    height: '100%',
    minHeight: '100vh',
  },
};

const colStackStyles = {
  root: {
    backgroundColor: '#F7F8FC',
    padding: 30,
    minWidth: 0,  // to avoid overflow
  },
};

const useForceUpdate = () => useState()[1];

const AdminLayout = ({ name, title, children }) => {
  const forceUpdate = useForceUpdate();

  useEffect(() => {
    window.addEventListener('resize', forceUpdate);

    return () => {
      window.removeEventListener('resize', forceUpdate);
    }
  }, [] );

  return (
      <Stack horizontal styles={rowStackStyles}>
          <SidebarComponent activeItem={name} />
          <Stack.Item grow={1} styles={colStackStyles} >
            <Stack verticalFill={true}>
              <Stack.Item>
                <HeaderComponent title={title} />
              </Stack.Item>
              <Stack.Item grow={1}>
                <div className={css(styles.content)}>
                  {children}
                </div>
              </Stack.Item>
            </Stack>
          </Stack.Item>
      </Stack>
  );
}

export default AdminLayout
