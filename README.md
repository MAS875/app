# Architecture

## Frontend
Javascript  
React  
Fluent UI (components, css in js)  
Redwood.js (fast creating pages, router)  
Apollo graphql client (in redwood)  

## Backend
Javascript  
FaaS (function as a service)  
Apollo graphql server  
Prisma 2 client  
Postgresql database  

## Deployment
Auto deployment to Netlify from Gitlab  

# Join development
## Git
Register a gitlab account  
`git clone ...` repository  
Create a new issue / take a created issue  
Create a new branch from issue, based on master branch  
Check out new branch  
Make changes  
Merge to master when ready  
Auto deployment to Netlify from master branch  

## Source codes
Both backend/frontend in same project  
api - backend codes  
web - frontend codes  

## Development environment
copy .env.defaults to .env  
Change DATABASE_URL  
`npm start` to start local server  

## Add entity or field to database
Edit api/prisma/schema.prisma
Run `yarn rw db save` to create migrations  
Run `yarn rw db up` to apply to database  
Edit api/graphql/people.sdl.js  
Change frontend codes  

## Generate random data
Edit api/prisma/seed.js  
Delete data from database  
`npm run seed`  
