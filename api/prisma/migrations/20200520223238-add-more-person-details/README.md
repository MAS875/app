# Migration `20200520223238-add-more-person-details`

This migration has been generated by Billy Leung at 5/20/2020, 10:32:38 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TYPE "ChristianStatus" AS ENUM ('catechumen', 'converted', 'baptized');

ALTER TABLE "public"."Person" ADD COLUMN "block" text   ,
ADD COLUMN "building" text   ,
ADD COLUMN "christianStatus" "ChristianStatus" NOT NULL DEFAULT 'catechumen',
ADD COLUMN "city" text   ,
ADD COLUMN "convertOn" timestamp(3)   ,
ADD COLUMN "country" text   ,
ADD COLUMN "district" text   ,
ADD COLUMN "estateStreet" text   ,
ADD COLUMN "floor" text   ,
ADD COLUMN "region" text   ,
ADD COLUMN "room" text   ;
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20200516180756-change-hkid-to-id-card-no..20200520223238-add-more-person-details
--- datamodel.dml
+++ datamodel.dml
@@ -1,7 +1,7 @@
 datasource DS {
   provider = "postgresql"
-  url = "***"
+  url      = env("DATABASE_URL")
 }
 generator client {
   provider      = "prisma-client-js"
@@ -12,25 +12,42 @@
   Male
   Female
 }
+enum ChristianStatus {
+  catechumen
+  converted
+  baptized
+}
+
 // Run `yarn rw db save` to create migrations
 // Run `yarn rw db up` to apply
 model Person {
-  id            String    @default(cuid()) @id
-  surname       String
-  givenName     String
-  surnameEn     String?
-  givenNameEn   String?
-  gender        Gender
-  phone         String?
-  phone2        String?
-  idCardNo      String?
-  email         String?   @unique
-  birthday      DateTime?
-  firstVisitOn  DateTime?
-  joinChurchOn  DateTime?
-  baptismOn     DateTime?
-  baptismChurch String?
-  createdAt     DateTime  @default(now())
-  updatedAt     DateTime  @updatedAt
+  id              String          @default(cuid()) @id
+  surname         String
+  givenName       String
+  surnameEn       String?
+  givenNameEn     String?
+  gender          Gender
+  phone           String?
+  phone2          String?
+  idCardNo        String?
+  email           String?         @unique
+  birthday        DateTime?
+  country         String?
+  city            String?
+  region          String?
+  district        String?
+  estateStreet    String?
+  building        String?
+  block           String?
+  floor           String?
+  room            String?
+  christianStatus ChristianStatus @default(catechumen)
+  firstVisitOn    DateTime?
+  joinChurchOn    DateTime?
+  convertOn       DateTime?
+  baptismOn       DateTime?
+  baptismChurch   String?
+  createdAt       DateTime        @default(now())
+  updatedAt       DateTime        @updatedAt
 }
```


