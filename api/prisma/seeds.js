/* eslint-disable no-console */
const { PrismaClient } = require('@prisma/client')
const dotenv = require('dotenv')
const faker = require('faker')
const randomName = require("chinese-random-name");
const OpenCC = require('opencc');
const converter = new OpenCC('s2t.json');

dotenv.config()
const db = new PrismaClient()

const firstPerson = {
  surname: "one",
  givenName: "initial",
  gender: 'Male',
}

const getRandomItem = items => items[Math.floor(Math.random() * items.length)]

async function main() {
  // Seed data is database data that needs to exist for your app to run.
  // Ideally this file should be idempotent: running it multiple times
  // will result in the same database state (usually by checking for the
  // existence of a record before trying to create it). For example:
  //
  //   const existing = await db.user.findMany({ where: { email: 'admin@email.com' }})
  //   if (!existing.length) {
  //     await db.user.create({ data: { name: 'Admin', email: 'admin@email.com' }})
  //   }

  // console.info('No data to seed. See api/prisma/seeds.js for info.')

  const existing = await db.person.findMany({ where: firstPerson })
  if (!existing.length) {
    await db.person.create({
      data: firstPerson
    })
    for (let i=0; i<1000; ++i) {
      const surname = await converter.convertPromise(randomName.surnames.getOne());
      const givenName = await converter.convertPromise(randomName.names.get2());
      const christian = faker.random.boolean();
      await db.person.create({
        data: {
          surname,
          givenName,
          surnameEn: faker.name.lastName(),
          givenNameEn: faker.name.firstName(),
          gender: getRandomItem([ 'Male', 'Female' ]),
          phone: faker.phone.phoneNumber(),
          email: faker.internet.email(),
          birthday: faker.date.past().toISOString(),
          firstVisitOn: faker.date.past().toISOString(),
          joinChurchOn: christian ? faker.date.past().toISOString() : undefined,
          baptismOn: christian ? faker.date.past().toISOString() : undefined,
          baptismChurch: christian ? faker.address.streetName() : undefined,
        },
      })
    }
  }
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await db.disconnect()
  })
