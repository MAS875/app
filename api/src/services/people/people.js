import { db } from 'src/lib/db'

export const people = ({ filter, orderBy = { surname: 'asc' }, offset, limit }) => {
  // if (orderBy?.name) {
  //   orderBy = { surname: orderBy.name, ...orderBy }
  //   delete orderBy['name']
  // }

  return {
    pageInfo: {
      totalCount: db.person.count()
    },
    nodes: db.person.findMany({
      skip: offset,
      first: limit,
      orderBy,
    })
  }
}

export const person = ({ id }) => {
  return db.person.findOne({
    where: { id },
  })
}

export const createPerson = ({ input }) => {
  console.log(input)
  return db.person.create({
    data: input,
  })
}

export const updatePerson = ({ id, input }) => {
  return db.person.update({
    data: input,
    where: { id },
  })
}

export const deletePerson = ({ id }) => {
  return db.person.delete({
    where: { id },
  })
}
