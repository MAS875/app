export const schema = gql`
  type Query {
    people(filter: String, orderBy: PersonOrderByInput, offset: Int!, limit: Int!): PeopleConnection!
    person(id: String!): Person!
  }

  type Mutation {
    createPerson(input: CreatePersonInput!): Person!
    updatePerson(id: String!, input: UpdatePersonInput!): Person!
    deletePerson(id: String!): Person!
  }

  type Person {
    id: String!
    surname: String!
    givenName: String!
    surnameEn: String
    givenNameEn: String
    gender: Gender!
    phone: String
    phone2: String
    idCardNo: String
    email: String
    birthday: DateTime
    country: String
    city: String
    region: String
    district: String
    estateStreet: String
    building: String
    block: String
    floor: String
    room: String
    christianStatus: ChristianStatus
    firstVisitOn: DateTime
    joinChurchOn: DateTime
    convertOn: DateTime
    baptismOn: DateTime
    baptismChurch: String
    createdAt: DateTime!
    updatedAt: DateTime!
  }

  input PersonOrderByInput {
    surname: OrderByArg
    surnameEn: OrderByArg
    gender: OrderByArg
    birthday: OrderByArg
    firstVisitOn: OrderByArg
    convertOn: DateTime
    joinChurchOn: OrderByArg
    baptismOn: OrderByArg
    createdAt: OrderByArg
  }

  type PeopleConnection {
    nodes: [Person!]!
    pageInfo: ConnectionPageInfo!
  }

  input CreatePersonInput {
    surname: String!
    givenName: String!
    surnameEn: String
    givenNameEn: String
    gender: Gender!
    phone: String
    phone2: String
    idCardNo: String
    email: String
    birthday: DateTime
    country: String
    city: String
    region: String
    district: String
    estateStreet: String
    building: String
    block: String
    floor: String
    room: String
    christianStatus: ChristianStatus
    firstVisitOn: DateTime
    convertOn: DateTime
    joinChurchOn: DateTime
    baptismOn: DateTime
    baptismChurch: String
  }

  input UpdatePersonInput {
    surname: String
    givenName: String
    surnameEn: String
    givenNameEn: String
    gender: Gender
    phone: String
    phone2: String
    idCardNo: String
    email: String
    birthday: DateTime
    country: String
    city: String
    region: String
    district: String
    estateStreet: String
    building: String
    block: String
    floor: String
    room: String
    christianStatus: ChristianStatus
    firstVisitOn: DateTime
    convertOn: DateTime
    joinChurchOn: DateTime
    baptismOn: DateTime
    baptismChurch: String
  }

  enum Gender {
    Male
    Female
  }

  enum ChristianStatus {
    catechumen
    converted
    baptized
  }

  type ConnectionPageInfo {
    totalCount: Int!
  }

  enum OrderByArg {
    asc
    desc
  }


`
