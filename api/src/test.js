const { PrismaClient, Gender } = require('@prisma/client')

process.env.DATABASE_URL= 'postgres://churchadmin:churchadmin@localhost:5432/churchadmin'
process.env.BINARY_TARGET='native'

const db = new PrismaClient()

db.person.findMany().then(result => {
  console.log(result);
})

db.person.create({
  data: {
    surname: "John",
    givenName: "Chan",
    gender: Gender.Male,
    phones: ["11111111","22222222"]
  }
}).then(result => {
  console.log(result);
})


