import AdminLayout from 'src/layouts/AdminLayout'
import SearchPeople from 'src/components/SearchPeople'

const SearchPersonPage = (props) => {
  return (
    <AdminLayout name="searchPeople" title="搜尋會友">
      <SearchPeople />
    </AdminLayout>
  )
}

export default SearchPersonPage
